#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CURRENT_DIR=`pwd`
INSTALLATION_TARGET="$HOME/.vim"
echo "Input vim runtime dir(default: $HOME/.vim)"
read LINE
if [[ $LINE != "" ]]
then
    INSTALLATION_TARGET=$LINE
fi

mkdir -p $INSTALLATION_TARGET

echo "Copying config files from ${SCRIPT_DIR}/myvimrc $INSTALLATION_TARGET"
eval "cp -r ${SCRIPT_DIR}/myvimrc ${INSTALLATION_TARGET}"

#create vim backups dir
mkdir -p $HOME/.vim/bck 2>/dev/null
mkdir -p $INSTALLATION_TARGET/myvimrc 2>/dev/null
mkdir -p $INSTALLATION_TARGET/myvimrc/bundle 2>/dev/null

echo "Seting .vimrc in $HOME (backuping old one as $HOME/.vimrc.bak)"
sudo cp -f $HOME/.vimrc $HOME/.vimrc.bak
sudo cp -f .vimrc $HOME/.vimrc

echo "Copying executables and configs to /opt/myvimrc"
sudo mkdir -p /opt/myvimrc/ 2>/dev/null
sudo cp -f .vimrc /opt/myvimrc/.vimrc
sudo cp -f $SCRIPT_DIR/bin/* /opt/myvimrc

echo "Setting up Vundle"
git clone https://github.com/VundleVim/Vundle.vim.git/ $INSTALLATION_TARGET/bundle/Vundle.vim

echo "Copying snippets from $SCRIPT_DIR/snippets to ${INSTALLATION_TARGET}/bundle/vim-snippets/snippets"
mkdir -p ${INSTALLATION_TARGET}/bundle/vim-snippets/snippets 2>/dev/null
eval "cp $SCRIPT_DIR/snippets/* ${INSTALLATION_TARGET}/bundle/vim-snippets/snippets"

echo "Linking binaries to /usr/bin"
sudo update-alternatives --install /usr/bin/gencpp gencpp /opt/myvimrc/gencpp.sh 1
sudo update-alternatives --install /usr/bin/jvim jvim /opt/myvimrc/jvim.sh 1
sudo update-alternatives --install /usr/bin/cvim cvim /opt/myvimrc/cvim.sh 1
sudo update-alternatives --install /usr/bin/getCPPForHeaderOrHeaderForCPP getCPPForHeaderOrHeaderForCPP /opt/myvimrc/getCPPForHeaderOrHeaderForCPP.sh 1
