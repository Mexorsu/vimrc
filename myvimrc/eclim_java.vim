nnoremap <C-i> :JavaImport<Enter>
nnoremap <C-o> :JavaImportOrganize<Enter>
nnoremap <C-j> :JavaDocPreview<Enter>
nnoremap <C-d> :JavaSearchContext<Enter>
nnoremap <C-r> :JavaSearch -s project -x references -p <c-r>=expand("<cword>")<cr><Enter>
