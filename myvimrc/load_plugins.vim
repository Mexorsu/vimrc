"Vundle settings (think plugins)
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'              " plugin manager
Plugin 'MarcWeber/vim-addon-mw-utils'   " dependency for snipmate
Plugin 'tomtom/tlib_vim'                " same as above
Plugin 'garbas/vim-snipmate'            " snipmate himself
Bundle 'lrvick/Conque-Shell'
Plugin 'scrooloose/nerdtree'            " Convenient tree-list file browser
Plugin 'ctrlpvim/ctrlp.vim'             " Fuzzy search engine for finding files
Plugin 'bling/vim-airline'              " eyecandy plugin (more visible tabs and buffer info)
Plugin 'vim-airline/vim-airline-themes' " airline plugin themes
Plugin 'sickill/vim-monokai'            " monokai theme
Plugin 'kana/vim-submode'               " user-defined sub-modes
Plugin 'easymotion/vim-easymotion'
Plugin 'jiangmiao/auto-pairs'
Plugin 'scrooloose/syntastic'
"Plugin 'fholgado/minibufexpl.vim'
call vundle#end()
filetype plugin indent on
"Vundle settings end

"Airline settings:
" - theme
let g:airline_theme='wombat'
" - show open buffers instead of tabs if theres only 1 tab
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#eclim#enabled = 1
" - submode config
let g:submode_always_show_submode='true'
let g:submode_timeoutlen='200000'
"Ctrlp settings:
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(exe|so|dll|class|o|swp|jar|war|ear)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }
let g:ctrlp_user_command = 'find %s -type f'
let g:ctrlp_cmd = 'CtrlP .'
let g:ctrlp_max_files = 0
