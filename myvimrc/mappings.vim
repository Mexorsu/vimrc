"Map leader to ,
let mapleader = ','

" Tabs switching:
" New tab with crtl + t
"nnoremap <C-T> :tabnew %.t<Enter>
nnoremap <C-T> :enew<Enter>
" Next tab with ctrl + page up
"nnoremap <C-PageUp> :tabnext<Enter>
nnoremap <C-PageUp> :bnext<Enter>
" Previous tab with ctrl + page down
"nnoremap <C-PageDown> :tabprevious<Enter>
nnoremap <C-PageDown> :bprev<Enter>

" Toggle whitespace view on F3
function! ShowWhitespaceOn()
    noremap <F3> :noh<return><esc>:call ShowWhitespaceOff()<Enter>
endfunction
function! ShowWhitespaceOff()
    noremap <F3> <Esc>/[ \t]\+<Enter>:call ShowWhitespaceOn()<Enter>
endfunction
noremap <F3> <Esc>/[ \t]\+<Enter>:call ShowWhitespaceOn()<Enter>

" Simplifies switching windows a bit (crtl + direction)
map <C-Up> <Esc><C-w>k
map <C-Down> <Esc><C-w>j
map <C-Left> <Esc><C-w>h
map <C-Right> <Esc><C-w>l

" Move lines with Alt+(Up/Down)...
nmap <M-Down> mz:m+<cr>`z
nmap <M-Up> mz:m-2<cr>`z
vmap <M-Down> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-Up> :m'<-2<cr>`>my`<mzgv`yo`z

" Close window with cc
nnoremap cc :q!<Enter>

" Two ctrl + c in rapid progress quit... like really quit..
nnoremap <C-c><C-c> :q!<Enter>:q!<Enter>:q!<Enter>:q!<Enter>:q!<Enter>:q!<Enter>:q!<Enter>:q!<Enter>:q!<Enter>:q!<Enter>:q!<Enter>

" Autocomplete with c-space
inoremap <C-Space> <C-X><C-U>
inoremap <C-@> <C-X><C-U>

" CD to current file directory
nnoremap <C-p> <Esc>:cd%:p:h<Enter>

" Vertical equivalent to :sbuffer
command! -nargs=1 Vsbuffer vert sb <args>

" Save with ctrl + s
"nnoremap <C-s> :w!<Enter>
inoremap <C-s> <c-o>:w!<Enter>

" Go to insert mode when backspacing or deleting
nmap <Backspace> i<Backspace>
nmap <Delete> i<Delete>

" Resize splits with + and - in normal mode
nmap + <c-w>+
nmap - <c-w>-
nmap > <c-w>>
nmap < <c-w><

if &diff
    set cursorline
    map ] ]c
    map [ [c
    hi DiffAdd    ctermfg=233 ctermbg=LightGreen guifg=#003300 guibg=#DDFFDD gui=none cterm=none
    hi DiffChange ctermbg=white  guibg=#ececec gui=none   cterm=none
    hi DiffText   ctermfg=233  ctermbg=yellow  guifg=#000033 guibg=#DDDDFF gui=none cterm=none
endif
