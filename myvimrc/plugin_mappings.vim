"NERDTree mappings:
" Open NERDTree on ctrl+n
map <C-n> :NERDTreeToggle<CR>
" Color files by etx (NERDTree)
runtime myvimrc/nerd_file_colors.vim

"SnipMate mappings
" Map SnipMate snippets to insert
:imap <Insert> <Plug>snipMateNextOrTrigger
:smap <Insert> <Plug>snipMateNextOrTrigger

"Conque mappings
" Conque console on crtl + c
nnoremap <C-b> :45sp<Enter><C-W><Down><Esc>:ConqueTerm zsh -is vim<Enter>
" ctrlp mappping
let g:ctrlp_map = '<c-f>'

map <c-k> \\w

" ECLIM mappings
runtime myvimrc/eclim_mappings.vim
"nnoremap <C-r> :JavaSearch -s all -x references<Enter>
runtime myvimrc/debug_mode.vim
