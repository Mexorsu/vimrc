let myvimrc_lang=$MYVIMRC_LANG

if myvimrc_lang == "CPP"
    runtime myvimrc/eclim_cpp.vim
else
    runtime myvimrc/eclim_java.vim
endif
