nnoremap <C-d> :CSearchContext<Enter>
nnoremap <C-r> :CSearch -s project -x references -p <c-r>=expand("<cword>")<cr><Enter>
nnoremap <c-h> :call OpenHeaderOrCpp()<Enter>
function! OpenHeaderOrCpp()
    if (&ft=='h'||&ft=='hpp'||&ft=='cpp')
        :let script="getCPPForHeaderOrHeaderForCPP ".expand("%")
        :let rev=system(script)
        :execute ':vsp '.rev
    endif
endfunction
