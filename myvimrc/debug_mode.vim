call submode#enter_with('debugging', 'n', '', '<c-g>', '<c-g>')
call submode#leave_with('debugging', 'n', '', '<Esc>')
call submode#map('debugging', 'n', '', '4', ':JavaDebugStep return<Enter>')
call submode#map('debugging', 'n', '', '5', ':JavaDebugStep over<Enter>')
call submode#map('debugging', 'n', '', '2', ':JavaDebugStep into<Enter>')
call submode#map('debugging', 'n', '', '6', ':JavaDebugThreadResume<Enter>')
call submode#map('debugging', 'n', '', 'r', ':JavaDebugStep return<Enter>')
call submode#map('debugging', 'n', '', 'o', ':JavaDebugStep over<Enter>')
call submode#map('debugging', 'n', '', 'i', ':JavaDebugStep into<Enter>')
call submode#map('debugging', 'n', '', 'c', ':JavaDebugThreadResume<Enter>')
call submode#map('debugging', 'n', '', 'bf', ':JavaDebugBreakpointsList<Enter>')
call submode#map('debugging', 'n', '', 'bb', ':JavaDebugBreakpointsList!<Enter>')
call submode#map('debugging', 'n', '', 's', ':JavaDebugStatus<Enter>')

call submode#map('debugging', 'n', '', '<Enter>', '<Enter>0')
"call submode#unmap('debugging', 'n', '', '<Enter>')
" Debugging:
" <
"nnoremap <Char-0x3C> <Esc>:JavaDebugStep return<Enter>
" >
"nnoremap <Char-0x3E> <Esc>:JavaDebugStep over<Enter>
" ?
"nnoremap <Char-0x3F> <Esc>:JavaDebugStep into<Enter>
" *
nnoremap * <Esc>:JavaDebugBreakpointToggle<Enter>
:command JDstart JavaDebugStart

