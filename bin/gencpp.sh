#!/bin/bash
if [[ $# > 0 ]]
then
    FILES_LIST=""
    for arg in "$@"
    do
        FILES_LIST="${FILES_LIST} ${arg}"
    done
else
    echo "No files list given!"
    exit 1
fi
for file in $FILES_LIST; do echo $file; done | \
    xargs -I % bash -c 'echo "file:%"; cat %' | \
    awk '
START{DO_PRINT=0}
/}/{DO_PRINT=0D}
/public:/{DO_PRINT=1}
/private:/{DO_PRINT=2}
/.*\(.*\)/{
    if(DO_PRINT==1) print "public:" $0;
    if(DO_PRINT==2) print "private:" $0;
}
/file:.*/{print $0}
' > tempfile
while read line
do
  if [[ "$line" =~ file:.* ]]
  then
    CPP_FILE_NAME=`echo $line | sed -e 's/file:\(\.\/\)\?\([^.]*\)\.h/\2.cpp/'`
    CLASS_NAME=`echo $CPP_FILE_NAME | sed -e 's/\.cpp//'`
    echo "Generating $CPP_FILE_NAME"
    rm ${CPP_FILE_NAME}.bak
    mv $CPP_FILE_NAME ${CPP_FILE_NAME}.bak
    echo "#include\"${CLASS_NAME}.h\"" > $CPP_FILE_NAME
  elif [[ "$line" =~ public:.* ]] || [[ "$line" =~ private:.* ]]
  then
    METHOD_NAME=`echo $line | sed -e 's/.*[ \t]\([a-zA-Z0-9~_]\+\)(.*/\1/'`
    RETURN_TYPE=`echo $line | sed -e "s/.*[ \t]\([a-zA-Z0-9]\+\)[ \t]\+$METHOD_NAME.*/\1/"`
    ARGS=`echo $line | sed -e 's/.*(\([^)]*\)).*/\1/'`
    MODIFIERS=`echo $line | sed -e "s/\(public\|private\):[ \t]*\(.*\)[ \t]\+\($RETURN_TYPE\|$METHOD_NAME\).*/\2/"`
    MODIFIERS=`echo $MODIFIERS | sed -e s/static//`
    MODIFIERS=`echo $MODIFIERS | sed -e s/virtual//`
    if [[ $MODIFIERS =~ (public|private).* ]]
    then
        MODIFIERS="";
    fi
    if [[ $RETURN_TYPE =~ (public|private).* ]]
    then
        RETURN_TYPE="";
    fi
    if [[ $METHOD_NAME =~ (public|private).* ]]
    then
        METHOD_NAME="";
    fi
    if [[ $ARGS =~ (public|private).* ]]
    then
        ARGS="";
    fi
    #if ! [[ $MODIFIERS =~ .*virtual.* ]]
    #then
        DEFINITION=`echo "$MODIFIERS $CLASS_NAME::$METHOD_NAME($ARGS)" | sed -e 's/^[ \t]*//'`
        echo $DEFINITION >> $CPP_FILE_NAME
        echo "{" >> $CPP_FILE_NAME
        echo "}" >> $CPP_FILE_NAME
    #fi
  fi
done < tempfile
rm tempfile
