#!/bin/bash
FILE_NAME=$1
if [[ $FILE_NAME =~ .*\.h ]]
then
    CPP_FILE_NAME=`echo $FILE_NAME | sed -e 's#\(.*/\?[^/]*\)\.h#\1.cpp#'`
    if ! [[ -f $CPP_FILE_NAME ]]
    then
        gencpp 1>/dev/null 2>/dev/null $FILE_NAME
    fi
    echo $CPP_FILE_NAME
elif [[ $FILE_NAME =~ .*\.cpp ]]
then
    echo $FILE_NAME | sed -e 's#\(.*/\?[^/]*\)\.cpp#\1.h#'
fi
