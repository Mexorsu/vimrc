" -----------------------
" BASIC SETTINGS
" -----------------------
" Go home grandpa vi, u drunk
set nocompatible
" Yeah, it's probably gonna be dark
set background=dark
" Show (partial) command in status line
set showcmd
" Show matching brackets
set showmatch
" Ignore case...
set ignorecase
" ...unless capitals used in pattern
set smartcase
" Automatically save before commands like :next and :make
set autowrite
" Adds line numbers
set number
" Remember that mach commands
set history=10000
" Show current line
set ruler
" Highliht search matches...
set hlsearch
" ...incrementally
set incsearch
" Don't redraw in macros (optimization)
set lazyredraw
set encoding=utf8
"set backupdir=~/.vim/bck," Redirect backups to ~/vim/bck
" Set omnifunc (autocompletion)
set omnifunc=syntaxcomplete#Complete
" Realy save cmd
":cmap w!! w !sudo tee %
" Tab = bunch of spaces
set expandtab
" and yeah, its 4 of them
set shiftwidth=4
set tabstop=4
" its smarth, though ;p
set smarttab
" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm
" make backspace not-suck
set backspace=eol,start,indent
" allow moving between lines with left and right arrows
set whichwrap+=<,>,[,]
if has("mouse")
	set mouse=a
endif
" Better filename completion
" more or less like in bash
if has("wildmenu")
	set wildmenu
	set wildmode=longest:full
endif
" No idea what below 3 are doing...
" ...looks smart though, keeping it
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif
"Syntax highlighting
syntax on
" Load basic mappings
runtime myvimrc/mappings.vim
" -----------------------
" LOADING OTHER SOURCES
" -----------------------
" Load plugins
runtime myvimrc/load_plugins.vim
" Set theme
colorscheme monokai
" Load custom plugin mappings
runtime myvimrc/plugin_mappings.vim
" Autogenerate defguards macros in h and hpp files
function! s:insert_gates()
  let gatename = substitute(toupper(expand("%:t")), "\\.", "_", "g")
  execute "normal! i#ifndef " . gatename
  execute "normal! o#define " . gatename . " "
  execute "normal! Go#endif /* " . gatename . " */"
  normal! kk
endfunction
autocmd BufNewFile *.{h,hpp} call <SID>insert_gates()
" Open splits on right
set splitright
